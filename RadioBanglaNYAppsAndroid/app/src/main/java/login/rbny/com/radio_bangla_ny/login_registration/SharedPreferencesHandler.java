package login.rbny.com.radio_bangla_ny.login_registration;


import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class SharedPreferencesHandler {

    SharedPreferences _sp;
    SharedPreferences.Editor editor;

    public SharedPreferencesHandler(Context context) {
        _sp = PreferenceManager.getDefaultSharedPreferences( context );
    }

    private static final String IS_LOGIN = "IsLoggedIn";

    public String getToken(String token) {
        return _sp.getString( "token", "" );
    }

    public void setToken(String token) {
        _sp.edit().putString( "token", token ).commit();
    }

    public String getName(String name) {

        return _sp.getString( "name", null );
    }

    public void setName(String name) {

        _sp.edit().putString( "name", name ).commit();
    }

    public String getEmail(String email) {

        return _sp.getString( "email", null );
    }

    public void setEmail(String email) {

        _sp.edit().putString( "email", email ).commit();
    }

    public String getId(String id) {

        return _sp.getString( "id", null );
    }

    public void setId(String id) {

        _sp.edit().putString( "id", id ).commit();
    }




    /*public Long getDuration(String duration) {

        return _sp.getLong( "duration", 30000 );
    }

    public void setDuration(Long duration) {
        _sp.edit().putLong( "duration", duration ).commit();
    }

    public String getNextUpcoming(String upcoming) {
        return _sp.getString( "upcoming", null );
    }

    public void setNextUpcoming(String upcoming) {

        _sp.edit().putString( "upcoming", upcoming ).commit();
    }*/

    public Integer getId(Integer id) {
        return _sp.getInt( "id", 0 );
    }

    public void setId(Integer id) {
        _sp.edit().putInt( "id", id ).commit();
    }

}


