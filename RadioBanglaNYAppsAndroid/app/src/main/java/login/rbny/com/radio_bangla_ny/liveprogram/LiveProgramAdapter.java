package login.rbny.com.radio_bangla_ny.liveprogram;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import login.rbny.com.radio_bangla_ny.R;

public class LiveProgramAdapter extends RecyclerView.Adapter<LiveProgramAdapter.MyViewHolder> {

    private List<LiveProgramData> liveProgram;
    //private List<HostList> hostLists;
    private Context context;
    private LayoutInflater inflater;

    public LiveProgramAdapter(Context context, List<LiveProgramData> liveProgram) {

        this.context = context;
        this.liveProgram = liveProgram;
        //this.hostLists = hostLists;
        inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
    }

    public LiveProgramAdapter(List<LiveProgramData> horizontalList) {
        this.liveProgram = horizontalList;

    }

    @Override
    public LiveProgramAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = inflater.inflate( R.layout.list_item, parent, false );
        return new LiveProgramAdapter.MyViewHolder( itemView );

    }

    @Override
    public void onBindViewHolder(final LiveProgramAdapter.MyViewHolder holder, final int position) {
        LiveProgramData upcoming = liveProgram.get( position );

        //holder.title.setText(upcoming.getTitle());
        holder.special_title.setText(upcoming.getSpecial_title());
        holder.summary.setText(upcoming.getSummary());

    }

    @Override
    public int getItemCount() {
        return liveProgram.size();

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView title,summary,special_title;

        public MyViewHolder(View itemView) {
            super( itemView );
            special_title = (TextView) itemView.findViewById( R.id.programtitle );
            summary = (TextView) itemView.findViewById( R.id.programsummary );

        }
    }
}

