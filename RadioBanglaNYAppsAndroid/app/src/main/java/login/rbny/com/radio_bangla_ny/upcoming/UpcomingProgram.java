package login.rbny.com.radio_bangla_ny.upcoming;

public class UpcomingProgram {

    private static String API_BASE_URL = "https://radiobangla.us/";
    private String imgURL, special_title;

    public UpcomingProgram(String special_title, String imgurl) {
        this.special_title = special_title;
        this.imgURL = imgurl;
    }

    public String getSpecial_title() {
        return special_title;
    }

    public String getImgURL() {
        if (imgURL != null){
            return API_BASE_URL+"uploads/programs/small/" + imgURL;
        } else {
            return API_BASE_URL+"uploads/images/default/program.jpg" ;
        }
    }
}
