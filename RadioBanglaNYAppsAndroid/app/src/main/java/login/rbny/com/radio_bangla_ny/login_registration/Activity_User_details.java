package login.rbny.com.radio_bangla_ny.login_registration;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import login.rbny.com.radio_bangla_ny.ListeningActivity;
import login.rbny.com.radio_bangla_ny.R;
import login.rbny.com.radio_bangla_ny.helper.SessionManager;

public class Activity_User_details extends Activity {


    private Button btnLogout;
    private SessionManager session;
    private String mResponse;
    SharedPreferencesHandler sp;
    TextView txtname,txtemail,txtid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_user_details );

        sp = new SharedPreferencesHandler( getApplicationContext() );

        btnLogout = (Button) findViewById( R.id.btnLogout );
        txtname = (TextView) findViewById( R.id.rbnyname );
        txtemail = (TextView) findViewById( R.id.rbnyemail );
        txtid = (TextView) findViewById( R.id.rbnyid );


        // SqLite database handler

        // session manager
        session = new SessionManager( getApplicationContext() );

        if (!session.isLoggedIn()) {
            logoutUser();
        }

        txtname.setText( sp.getName( "name" ) );
        txtemail.setText( sp.getEmail( "email" ) );
        txtid.setText( sp.getId( "id" ) );

        SharedPreferences m = PreferenceManager.getDefaultSharedPreferences( this );
        mResponse = m.getString( "Response", "test" );

        btnLogout.setOnClickListener( v -> logoutUser() );
    }
    private void logoutUser() {

        session.setLogin( false );
        Intent intent = new Intent( Activity_User_details.this, ListeningActivity.class );
        startActivity( intent );
        finish();
    }
}
