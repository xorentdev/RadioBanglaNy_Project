package login.rbny.com.radio_bangla_ny.volly;


import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class CustomGetPostRequest extends Request<JSONObject> {
    private int mMethod;
    private String mUrl;
    Map<String, String> mParams = new HashMap<String, String>();
    private Response.Listener<JSONObject> mListener;
    HashMap<String, String> headers = new HashMap<String, String>();

    public CustomGetPostRequest(int method, String url, Map<String, String> params,
                                Response.Listener<JSONObject> reponseListener, Response.ErrorListener errorListener) {
        super( method, url, errorListener );
        mMethod = method;
        mUrl = url;
        Log.d( "Main URL", mUrl );
        mParams = params;
        mListener = reponseListener;
    }

    @Override
    public String getUrl() {
        if (mMethod == Request.Method.GET) {
            StringBuilder stringBuilder = new StringBuilder( mUrl );
            Iterator<Map.Entry<String, String>> iterator = mParams.entrySet().iterator();
            int i = 1;
            while (iterator.hasNext()) {
                Map.Entry<String, String> entry = iterator.next();
                if (i == 1) {
                    stringBuilder.append( "?" + entry.getKey() + "=" + entry.getValue() );
                } else {
                    stringBuilder.append( "&" + entry.getKey() + "=" + entry.getValue() );
                }
                iterator.remove(); // avoids a ConcurrentModificationException
                i++;
            }
            mUrl = stringBuilder.toString();
            Log.d( "Converted URL", mUrl );
        }
        return mUrl;
    }

    @Override
    protected Map<String, String> getParams()
            throws com.android.volley.AuthFailureError {
        Log.d( "getParams", "Called" );
        return mParams;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
//        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put( "Content-Type", "application/json; charset=utf-8" );
//        headers.put ("Content-Type", "application/x-www-form-urlencoded");
        return headers;
    }

    @Override
    protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
        try {

            String jsonString = new String( response.data,
                    HttpHeaderParser.parseCharset( response.headers ) );
            return Response.success( new JSONObject( jsonString ),
                    HttpHeaderParser.parseCacheHeaders( response ) );
        } catch (UnsupportedEncodingException e) {
            return Response.error( new ParseError( e ) );
        } catch (JSONException je) {
            if (response.statusCode == 200)// Added for 200 response
                return Response.success( new JSONObject(), HttpHeaderParser.parseCacheHeaders( response ) );
            else
                return Response.error( new ParseError( je ) );
        }
    }

    @Override
    protected void deliverResponse(JSONObject response) {
        // TODO Auto-generated method stub
        mListener.onResponse( response );
    }
}