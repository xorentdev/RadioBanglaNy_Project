package login.rbny.com.radio_bangla_ny.upcoming;

import android.support.v7.widget.RecyclerView;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;

import java.util.List;

import login.rbny.com.radio_bangla_ny.R;

public class UpcomingAdapter extends RecyclerView.Adapter<UpcomingAdapter.MyViewHolder> {

    private List<UpcomingProgram> upcomingList;
    private Context context;
    private LayoutInflater inflater;

    public UpcomingAdapter(Context context, List<UpcomingProgram> upcomingList) {

        this.context = context;
        this.upcomingList = upcomingList;

        inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
    }

    public UpcomingAdapter(List<UpcomingProgram> horizontalList) {
        this.upcomingList = horizontalList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = inflater.inflate( R.layout.upcoming_recyclerview, parent, false );
        return new UpcomingAdapter.MyViewHolder( itemView );
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        UpcomingProgram program = upcomingList.get( position );
        //Pass the values of feeds object to Views
        holder.special_title.setText( program.getSpecial_title() );
        holder.imageview.setImageUrl( program.getImgURL(), NetworkController_upcoming.getInstance( context ).getImageLoader() );
    }

    @Override
    public int getItemCount() {
        return upcomingList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView special_title;
        private NetworkImageView imageview;

        public MyViewHolder(View itemView) {
            super( itemView );
            special_title = (TextView) itemView.findViewById( R.id.s_title );
            // Volley's NetworkImageView which will load Image from URL
            imageview = (NetworkImageView) itemView.findViewById( R.id.thumbnail );
        }
    }

}
