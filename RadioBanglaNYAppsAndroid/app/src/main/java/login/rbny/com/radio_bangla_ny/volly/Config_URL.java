package login.rbny.com.radio_bangla_ny.volly;

//This class is for storing all URLs as a model of URLs

public class Config_URL {

	public static String URL_LOGIN = "https://radiobangla.us/api/login";

	public static String URL_DETAILS = "https://radiobangla.us/api/details";

	public static String URL_REGISTER = "https://radiobangla.us/api/register";

	public static String URL_LIKE = "https://radiobangla.us/api/like/program/id";

}
