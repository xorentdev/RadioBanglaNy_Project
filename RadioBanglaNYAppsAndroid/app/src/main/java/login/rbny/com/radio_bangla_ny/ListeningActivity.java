package login.rbny.com.radio_bangla_ny;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonArrayRequest;
import com.onesignal.OneSignal;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import login.rbny.com.radio_bangla_ny.helper.HttpHandler;
import login.rbny.com.radio_bangla_ny.helper.SessionManager;
import login.rbny.com.radio_bangla_ny.livehost.LiveHostsAdapter;
import login.rbny.com.radio_bangla_ny.livehost.LiveHostsData;
import login.rbny.com.radio_bangla_ny.livehost.NetworkController_live;
import login.rbny.com.radio_bangla_ny.login_registration.Activity_Login;
import login.rbny.com.radio_bangla_ny.service.MyService;

public class ListeningActivity extends Activity {

    private static String STREAM_URL = "https://stream.radio.co/s57864048c/listen";
    private static String API_BASE_URL = "https://radiobangla.us/";
    private Button btnPlay1;
    private Button btnPlay2;
    private Button btnShare;
    private Button btnPower;
    private Button btnBack;
    private Button btnLike;
    private Button btnUp;
    private RelativeLayout myRelativeLauout;
    private String dynamicImageUrl;
    private String dynamicShareUrl, message;
    private ImageView moreShare;
    private boolean isLive = false;
    private boolean isHostActive = false;
    private boolean checkedLike = true;
    private SessionManager session;
    private static MainActivity mainActivity;


    MediaPlayer mediaPlayer;
    boolean prepared = false;
    boolean started = false;

    RequestQueue queue;
    String url = API_BASE_URL + "api/program/live";
    RecyclerView recyclerview;
    List<LiveHostsData> liveHostsList = new ArrayList<LiveHostsData>();
    LiveHostsAdapter adapterLiveHosts;
    LinearLayoutManager HorizontalLayout;
    RecyclerView.LayoutManager RecyclerViewLayoutManager;
    Handler mHandler = new Handler();


    private String TAG = ListeningActivity.class.getSimpleName();
    ArrayList<HashMap<String, String>> programList;
    private ListView listView;
    private PlayerTask task;
    Long startTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listening);


        // Show time
        startTime = System.currentTimeMillis();
        // Show progress dialogue
        Pdialog.showProgressDialog(this, "Loading...");


        mediaPlayer = new MediaPlayer();

        radioBanglalive();

        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();

                /*For some kinds of onclick:Backbutton,Player,Like,Upbutton,powerbutton,Sharebutton*/

        btnLike = findViewById(R.id.btn_like);
        btnLike.setOnClickListener(v -> {
            if (checkedLike == true) {
                btnLike.setBackgroundResource(R.drawable.like_button2);
                checkedLike = false;
                Intent m = new Intent(ListeningActivity.this, Activity_Login.class);
                ListeningActivity.this.startActivity(m);
            } else {
                btnLike.setBackgroundResource(R.drawable.like_button1);
                btnLike.setTag(70);
                checkedLike = true;
                //Toast.makeText(getApplicationContext(), "DisLiked!", Toast.LENGTH_SHORT ).show();
            }

        });

        programList = new ArrayList<>();
        listView = findViewById(R.id.listview3);
        new GetPrograms().execute();

        /*Background change for RelativeLayout*/

        myRelativeLauout = findViewById(R.id.activity_listening);

        btnBack = findViewById(R.id.btn_back);
        btnBack.setOnClickListener(view -> {
            Intent monad = new Intent(ListeningActivity.this, HomeScreenActivity.class);
            ListeningActivity.this.startActivity(monad);
        });

        btnUp = findViewById(R.id.btn_upcoming);
        btnUp.setOnClickListener(view -> {
            Intent monad = new Intent(ListeningActivity.this, HomeScreenActivity.class);
            ListeningActivity.this.startActivity(monad);
        });


        /*close activity press power button*/

        btnPower = findViewById(R.id.btn_power);
        btnPower.setOnClickListener(view -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(ListeningActivity.this);
            builder.setIcon(R.drawable.warning_black);
            builder.setTitle("EXIT");
            builder.setMessage("Are You Exit?")
                    .setPositiveButton("Cancel", (dialog, id) -> dialog.dismiss())
                    .setNegativeButton("Yes", (dialog, id) -> {
                        Intent intent = new Intent("custom-event-name");
                        intent.putExtra("message", "This is my message!");
                        LocalBroadcastManager.getInstance(ListeningActivity.this).sendBroadcast(intent);
                        finish();
                    });
            builder.show();
        });


        btnShare = findViewById(R.id.btn_share);
        btnShare.setOnClickListener(v -> {
            Intent myIntent = new Intent();
            myIntent.setAction(Intent.ACTION_SEND);
            myIntent.putExtra(Intent.EXTRA_TEXT, dynamicShareUrl);
            myIntent.setType("text/plain");
            ListeningActivity.this.startActivity(myIntent);
        });
        moreShare = findViewById(R.id.imageView14);
        moreShare.setOnClickListener(view -> {
            Intent myIntent = new Intent();
            myIntent.setAction(Intent.ACTION_SEND);
            myIntent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=login.rbny.com.radio_bangla_ny");
            myIntent.setType("text/plain");
            ListeningActivity.this.startActivity(myIntent);
        });

        /*Onclick Play buttons*/

        btnPlay2 = findViewById(R.id.btn_play1);
        btnPlay2.setEnabled(false);
        btnPlay1 = findViewById(R.id.listen_play2);
        btnPlay1.setEnabled(false);


        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);


        //new PlayerTask().execute(STREAM_URL); not using this asyntask
        btnPlay2.setOnClickListener(view -> {


            if (PlayAudio.isplayingAudio) {
                PlayAudio.pause(); //song_circle
                btnPlay2.setBackgroundResource(R.drawable.play_button1);
                btnPlay1.setBackgroundResource(R.drawable.ic_play_button);

            } else {
                PlayAudio.start();
                btnPlay2.setBackgroundResource(R.drawable.stop_button1);
                btnPlay1.setBackgroundResource(R.drawable.stop_button);
                ListeningActivity.this.stopService(new Intent(ListeningActivity.this, MyService.class));
            }
        });
        btnPlay1.setOnClickListener(view -> {

            if (PlayAudio.isplayingAudio) {
                PlayAudio.pause();  //song_circle
                btnPlay2.setBackgroundResource(R.drawable.play_button1);
                btnPlay1.setBackgroundResource(R.drawable.ic_play_button);


            } else {
                PlayAudio.start();
                btnPlay2.setBackgroundResource(R.drawable.stop_button1);
                btnPlay1.setBackgroundResource(R.drawable.stop_button);
                ListeningActivity.this.stopService(new Intent(ListeningActivity.this, MyService.class));
            }
        });

        new Thread(() -> {
            while (true) {
                try {
                    Thread.sleep(30000);
                    mHandler.post(() -> {
                        new GetPrograms().execute();
                        if (!isLive) {
                            programList.clear();
                            liveHostsList.clear();
                            radioBanglalive();
                            myRelativeLauout.setBackgroundResource(R.drawable.rbny_bg1);
                        } else if (!isHostActive) {
                            radioBanglalive();
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }
    /* For Live program List */


    public void radioBanglalive() {

        recyclerview = findViewById(R.id.recyclerview);
        adapterLiveHosts = new LiveHostsAdapter(this, liveHostsList);
        recyclerview.setLayoutManager(new LinearLayoutManager(this));
        RecyclerViewLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerview.setLayoutManager(RecyclerViewLayoutManager);

        // Adding items to RecyclerView.
        HorizontalLayout = new LinearLayoutManager(ListeningActivity.this, LinearLayoutManager.HORIZONTAL, false);
        recyclerview.setLayoutManager(HorizontalLayout);
        recyclerview.setAdapter(adapterLiveHosts);
        //Getting Instance of Volley Request Queue
        queue = NetworkController_live.getInstance(this).getRequestQueue();

        //Volley's inbuilt class to make Json array request
        final JsonArrayRequest liveReq = new JsonArrayRequest(url, response -> {
            Log.e(TAG, "Response from url: " + response);

            try {
                isHostActive = true;
                JSONObject objProgram = response.getJSONObject(0);
                JSONArray arrayHosts = objProgram.getJSONArray("hosts");
                for (int j = 0; j < arrayHosts.length(); j++) {
                    JSONObject objHost = arrayHosts.getJSONObject(j);
                    JSONObject objProfile = objHost.getJSONObject("profile");
                    String username = objProfile.getString("display_name");
                    LiveHostsData liveHosts = new LiveHostsData(username, objProfile.getString("image"));
                    liveHostsList.add(liveHosts);

                }
            } catch (Exception e) {
                System.out.println("We will right border");

            } finally {
                //Notify adapterLiveHosts about data changes
                adapterLiveHosts.notifyItemChanged(0);
            }

        }, error -> {
            System.out.println(error.getMessage());
            Toast.makeText(ListeningActivity.this.getApplicationContext(), "In live program we will be back in time:", Toast.LENGTH_LONG).show();

           /* message = "In live program we will be back in time:";
            error_popup();*/
        });

        queue.add(liveReq);
    }

    /*MediaPlayer1 Task Music Streaming*/

    private class PlayerTask extends AsyncTask<String, Void, Boolean> {
        @Override
        protected void onPreExecute() {

            super.onPreExecute();



           /* progress = new ProgressDialog(ListeningActivity.this);
            progress.setMessage("Loading...");
            progress.setCancelable(false);
            progress.show();*/
        }

        @Override
        protected Boolean doInBackground(String... strings) {


            prepared = true;
           /* try {

              *//*  mediaPlayer.setDataSource( strings[0] );
                mediaPlayer.prepareAsync();*//*
                prepared = true;
                //mediaPlayer.start();
            } catch (IOException e) {
                mediaPlayer.reset();
                e.printStackTrace();
            }*/
            return null;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            //mediaPlayer.start();
            btnPlay2.setBackgroundResource(R.drawable.stop_button1);
            //mediaPlayer.start();
            btnPlay1.setBackgroundResource(R.drawable.stop_button);
            btnPlay2.setEnabled(true);
            btnPlay1.setEnabled(true);
            //  progress.dismiss();


        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (started) {
            PlayAudio.start();
            //mediaPlayer.start();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (started) {
            PlayAudio.stop();
            // mediaPlayer.stop();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (started) {
            PlayAudio.start();
            //mediaPlayer.start();
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (prepared) {
            PlayAudio.release();
            // mediaPlayer.release();
        }
       /* if (progress != null && progress.isShowing()) {
            progress.cancel();
        }*/

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(ListeningActivity.this, HomeScreenActivity.class);
        startActivity(intent);
    }


    /*MediaPlayer1 Current Program List*/

    public class GetPrograms extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpHandler sh = new HttpHandler();
            // Making a request to url and getting response
            String url = API_BASE_URL + "api/program/live";
            String jsonStr = sh.makeServiceCall(url);
            Log.e(TAG, "Response from url: " + jsonStr);
            if (jsonStr != null) {
                try {
                    isLive = true;
                    JSONArray jsonArray = new JSONArray(jsonStr);
                    JSONObject obj = jsonArray.getJSONObject(0);

                    String special_title = obj.getString(obj.has("special_title") ? "special_title" : "title");
                    String summary = obj.has("summary") ? obj.getString("summary") : "";
                    summary = summary.length() >= 30 ? summary.substring(0, 30) + "..." : summary;
                    dynamicShareUrl = obj.has("id") ? "https://www.radiobanglany.com/program/" + obj.getString("id") + "/" + special_title.toLowerCase().replaceAll("[^a-z0-9-]", "-") : "https://play.google.com/store/apps/details?id=login.rbny.com.radio_bangla_ny";
                    dynamicImageUrl = obj.has("image") ? API_BASE_URL + "uploads/programs/small/" + obj.getString("image") : dynamicImageUrl;
                    new LoadBackground(dynamicImageUrl, "androidfigure").execute();

                    HashMap<String, String> live = new HashMap<>();
                    live.put("special_title", special_title);
                    live.put("summary", summary);

                    programList.add(live);

                  dismissdialog();

                } catch (final JSONException e) {
                    isLive = false;
                    Log.e(TAG, "" + e.getMessage());

                    runOnUiThread(() -> {
                        isLive = false;
                        LiveHostsData liveHosts = new LiveHostsData("Automated", null);
                        liveHostsList.add(liveHosts);
                    });

                    dismissdialog();


                }
            } else {
                isLive = false;
                Log.e(TAG, "api response error!"+jsonStr);
                //   runOnUiThread(() -> Toast.makeText(getApplicationContext(), "Check your internet Connection!", Toast.LENGTH_SHORT).show());

                message = "Check your internet Connection!";
                error_popup();
                dismissdialog();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            ListAdapter adapter = new SimpleAdapter(ListeningActivity.this, programList,
                    R.layout.list_item, new String[]{"special_title", "summary"},
                    new int[]{R.id.programtitle, R.id.programsummary});
            listView.setAdapter(adapter);
        }
    }

    /*Load current Background from url*/

    private class LoadBackground extends AsyncTask<String, Void, Drawable> {

        private String imageUrl, imageName;

        LoadBackground(String url, String file_name) {
            this.imageUrl = url;
            this.imageName = file_name;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Drawable doInBackground(String... urls) {
            try {
                InputStream is = (InputStream) this.fetch(this.imageUrl);
                Drawable d = Drawable.createFromStream(is, this.imageName);
                return d;
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return null;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }

        }

        private Object fetch(String address) throws IOException {
            URL url = new URL(address);
            Object content = url.getContent();
            return content;
        }

        @Override
        protected void onPostExecute(Drawable result) {
            super.onPostExecute(result);
            myRelativeLauout.setBackgroundDrawable(result);
            myRelativeLauout.getBackground().setAlpha(80);
        }
    }

    /*For Parrallely load AsyncTask*/

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void StartAsyncTaskInParallel(PlayerTask task, String STREAM_URL) {
        this.task = task;
        this.STREAM_URL = STREAM_URL;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, url);
        else
            task.execute();
    }

    // Looping for media player true or faslse once coming  true progress dialogue is dissmissed
    public void dismissdialog() {

        while (true) {
            if (PlayAudio.isdialog) {
                Pdialog.dismissprogress(ListeningActivity.this);
                PlayAudio.isdialog=false;
                String totalTimeInMs = String.valueOf(System.currentTimeMillis() - startTime);
                Log.d(TAG, "doInBackground: loader time in ms:" + totalTimeInMs);


                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {


                        // Enable buttons
                        if (PlayAudio.isplayingAudio) {
                            btnPlay2.setBackgroundResource(R.drawable.stop_button1);
                            btnPlay1.setBackgroundResource(R.drawable.stop_button);
                            btnPlay2.setEnabled(true);
                            btnPlay1.setEnabled(true);
                            prepared = true;

                        } else {
                            btnPlay2.setBackgroundResource(R.drawable.stop_button1);
                            btnPlay1.setBackgroundResource(R.drawable.stop_button);
                            btnPlay2.setEnabled(true);
                            btnPlay1.setEnabled(true);
                            prepared = true;
                        }

                    }
                });

                ListeningActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                      //  Toast.makeText(ListeningActivity.this, "Loader time in " + totalTimeInMs + " Sec", Toast.LENGTH_SHORT).show();
                    }
                });

                break;
            }
        }
    }

    public void error_popup() {

        if (!isFinishing()) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {


                    AlertDialog.Builder builder1 = new AlertDialog.Builder(ListeningActivity.this);
                    builder1.setMessage(message);
                    builder1.setCancelable(true);

                    builder1.setPositiveButton(
                            "OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    finish();
                                }
                            });
                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                }
            });

        }


    }
}

