package login.rbny.com.radio_bangla_ny;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.method.LinkMovementMethod;
import android.util.Log;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.onesignal.OneSignal;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import login.rbny.com.radio_bangla_ny.livehost.NetworkController_live;
import login.rbny.com.radio_bangla_ny.login_registration.SharedPreferencesHandler;
import login.rbny.com.radio_bangla_ny.upcoming.UpcomingProgram;
import login.rbny.com.radio_bangla_ny.upcoming.UpcomingAdapter;


public class HomeScreenActivity extends Activity {

    private Button btn_power2;
    private Button btn_playing;
    private TextView mLink;
    private ImageView imageView;
    private static final String TAG = HomeScreenActivity.class.getSimpleName();

    RequestQueue queue;
    String url = "https://radiobangla.us/api/program/upcoming";
    RecyclerView recyclerview;
    ArrayList<UpcomingProgram> upcomingList = new ArrayList<UpcomingProgram>();
    UpcomingAdapter adapter;
    String position;
    LinearLayoutManager HorizontalLayout;
    RecyclerView.LayoutManager RecyclerViewLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_home_screen );
        ucomingProgram();

        OneSignal.startInit( this )
                .inFocusDisplaying( OneSignal.OSInFocusDisplayOption.Notification )
                .unsubscribeWhenNotificationsAreDisabled( true )
                .init();

        /*For some kinds of onclick:Power,Playing,Link,*/

        mLink = findViewById( R.id.link );
        if (mLink != null) {
            mLink.setMovementMethod( LinkMovementMethod.getInstance() );
        }

        LocalBroadcastManager.getInstance( this ).registerReceiver( mMessageReceiver, new IntentFilter( "custom-event-name" ) );

        btn_power2 = findViewById( R.id.btn_power2 );
        btn_power2.setOnClickListener( view -> HomeScreenActivity.this.finish() );

        btn_playing = findViewById( R.id.btn_playing );
        btn_playing.setOnClickListener( view -> {
            Intent j = new Intent( HomeScreenActivity.this, ListeningActivity.class );
            HomeScreenActivity.this.startActivity( j );
        } );

        imageView = findViewById( R.id.imageView7 );
        imageView.setOnClickListener( view -> {
            Intent myIntent = new Intent();
            myIntent.setAction( Intent.ACTION_SEND );
            myIntent.putExtra( Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=login.rbny.com.radio_bangla_ny" );
            myIntent.setType( "text/plain" );
            startActivity( myIntent );
        } );

    }
        /* For Upcoming program List */

    public void ucomingProgram() {
        recyclerview = findViewById( R.id.recyclerview2 );
        adapter = new UpcomingAdapter( this, upcomingList );
        recyclerview.setLayoutManager( new LinearLayoutManager( this ) );
        RecyclerViewLayoutManager = new LinearLayoutManager( getApplicationContext() );

        recyclerview.setLayoutManager( RecyclerViewLayoutManager );

        HorizontalLayout = new LinearLayoutManager( HomeScreenActivity.this, LinearLayoutManager.HORIZONTAL, false );
        recyclerview.setLayoutManager( HorizontalLayout );

        recyclerview.setAdapter( adapter );
        //Getting Instance of Volley Request Queue
        queue = NetworkController_live.getInstance( this ).getRequestQueue();
        //Volley's inbuilt class to make Json array request
        final JsonArrayRequest ucomingReq = new JsonArrayRequest( url, response -> {


            for (int i = 0; i < response.length(); i++) {
                try {
                    JSONObject obj = response.getJSONObject( i );
                    /*if(i==0){
                        sp.setNextUpcoming(obj.getString( "start" ));
                    }*/
                    String special_title = obj.getString( obj.has( "special_title" ) ? "special_title" : "title" );
                    UpcomingProgram upcoming = new UpcomingProgram( special_title, obj.has( "image" ) ? obj.getString( "image" ) : null );
                    // adding data to upcomingList array
                    upcomingList.add( upcoming );

                } catch (JSONException e) {
                    System.out.println( e.getMessage() );

                } finally {
                    //Notify adapter about data changes
                    adapter.notifyItemChanged( i );
                }
            }

        }, error -> {
            System.out.println( error.getMessage() );
            Toast.makeText( HomeScreenActivity.this.getApplicationContext(), "Check your internet Connection!", Toast.LENGTH_LONG ).show();
        } );
        queue.add( ucomingReq );
    }

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance( this ).unregisterReceiver( mMessageReceiver );
        super.onDestroy();
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String message = intent.getStringExtra( "message" );
            Log.d( "receiver", "Got message: " + message );
            finish();
        }
    };

    public void onBackPressed() {
        moveTaskToBack( true );
        Toast.makeText( getApplicationContext(), "Press Power Button On The Right Corner! ", Toast.LENGTH_LONG ).show();
    }
}




