package login.rbny.com.radio_bangla_ny.liveprogram;

public class LiveProgramData {

    private String title,special_title;
    private String summary;

    public LiveProgramData(String title,String special_title ,String summary) {
        this.title = title;
        this.special_title = special_title;
        this.summary = summary;

    }

    public String getTitle() {
        return title;
    }
    public String getSummary() {
        return summary;
    }

    public String getSpecial_title() {
        return special_title;
    }
}
