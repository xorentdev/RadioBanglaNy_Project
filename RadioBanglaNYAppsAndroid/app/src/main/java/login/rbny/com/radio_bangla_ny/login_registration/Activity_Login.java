package login.rbny.com.radio_bangla_ny.login_registration;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;
import login.rbny.com.radio_bangla_ny.R;
import login.rbny.com.radio_bangla_ny.helper.SessionManager;
import login.rbny.com.radio_bangla_ny.volly.AppController;
import login.rbny.com.radio_bangla_ny.volly.Config_URL;


public class Activity_Login extends Activity {

    // LogCat tag
    private static final String TAG = Activity_Register.class.getSimpleName();
    private Button btnLogin;
    private Button btnLinkToRegister;
    private EditText inputEmail;
    private EditText inputPassword;
    private ProgressDialog pDialog;
    private SessionManager session;
    private CallbackManager mCallbackManager;
    private LoginButton mloginButton;
    private Button custombtn;
    SharedPreferencesHandler sp;
    private Button btnfacebook;
    LoginManager mLoginManager;
    FragmentManager mFragments;
    TextView txtview;
    AccessTokenTracker mAccessTokenTracker;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        FacebookSdk.sdkInitialize( getApplicationContext() );
        AppEventsLogger.activateApp( this );
        setContentView( R.layout.activity_login );

        android.app.FragmentManager fm = getFragmentManager();
        android.app.Fragment fragment = fm.findFragmentById( R.id.fragment_container );

        btnfacebook = (Button) findViewById( R.id.btn_facebook );
        btnfacebook.setOnClickListener( view -> {
            android.app.Fragment someFragment = new android.app.Fragment();
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.replace( R.id.fragment_container, someFragment ); // give your fragment container id in first parameter
            transaction.addToBackStack( null );  // if written, this transaction will be added to backstack
            transaction.commit();
        } );

        if (fragment == null) {
            fragment = new android.app.Fragment();
            fm.beginTransaction()
                    .add( R.id.fragment_container, fragment )
                    .commit();
        }
        sp = new SharedPreferencesHandler( getApplicationContext() );

        inputEmail = (EditText) findViewById( R.id.email );
        inputPassword = (EditText) findViewById( R.id.password );
        btnLogin = (Button) findViewById( R.id.btnLogin );
        btnLinkToRegister = (Button) findViewById( R.id.btnLinkToRegisterScreen );

        // Progress dialog
        pDialog = new ProgressDialog( this );
        pDialog.setCancelable( false );

        // Session manager
        session = new SessionManager( getApplicationContext() );

        // Check if user is already logged in or not
        if (session.isLoggedIn()) {
            // User is already logged in. Take him to main activity
            Intent tsm = new Intent( Activity_Login.this, Activity_User_details.class );
            startActivity( tsm );
            finish();
        }

        // Login button Click Event
        btnLogin.setOnClickListener( view -> {
            String email = inputEmail.getText().toString();
            String password = inputPassword.getText().toString();

            // Check for empty data in the form
            if (email.trim().length() > 0 && password.trim().length() > 0) {
                // login user
                checkLogin( email, password );
                Intent i = new Intent( Activity_Login.this, Activity_User_details.class );
                startActivity( i );
                finish();
                Toast.makeText( getApplicationContext(),
                        "You liked Successfully!", Toast.LENGTH_SHORT )
                        .show();

            } else {
                // Prompt user to enter credentials
                Toast.makeText( getApplicationContext(),
                        "Please enter the credentials!", Toast.LENGTH_LONG )
                        .show();
            }
        } );

        // Link to Register Screen
        btnLinkToRegister.setOnClickListener( view -> {
            Intent i = new Intent( getApplicationContext(), Activity_Register.class );
            startActivity( i );
            finish();
        } );

        /*Intent myIntent = new Intent(Activity_Login.this, UserDetailsActivity.class);
        myIntent.putExtra("key", "value");
        startActivity(myIntent);*/


       /* mCallbackManager = CallbackManager.Factory.create();
        mloginButton = (LoginButton) findViewById( R.id.facebook_sign_in_button );
        txtview = (TextView) findViewById( R.id.txtview );
        custombtn = (Button) findViewById( R.id.btn_facebook );

        mloginButton.registerCallback( mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                txtview.setText( "Login Success \n" + loginResult.getAccessToken().getUserId() + "\n" +
                        loginResult.getAccessToken().getToken() );
            }

            @Override
            public void onCancel() {
                txtview.setText( "Login Cancelled" );

            }

            @Override
            public void onError(FacebookException error) {

            }
        } );

        LoginManager.getInstance().registerCallback( mCallbackManager, new FacebookCallback<LoginResult>() {


            @Override
            public void onSuccess(LoginResult loginResult) {
                txtview.setText( "Login Success \n" + loginResult.getAccessToken().getUserId() + "\n" +
                        loginResult.getAccessToken().getToken() );
            }

            @Override
            public void onCancel() {
                txtview.setText( "Login Cancelled" );
            }

            @Override
            public void onError(FacebookException error) {

            }
        } );

        custombtn.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LoginManager.getInstance().logInWithReadPermissions( Activity_Login.this, Arrays.asList( "public_profile","email","user_friends" ) );
                LoginManager.getInstance().logOut();
            }
        } );*/

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        mCallbackManager.onActivityResult( requestCode, resultCode, data );
    }

    private void checkLogin(final String email, final String password) {
        // Tag used to cancel the request
        final String tag_string_req1 = "req_login";
        pDialog.setMessage( "Logging in ..." );
        showDialog();

        final StringRequest strReq1 = new StringRequest( Request.Method.POST,
                Config_URL.URL_LOGIN, response -> {
            Log.d( TAG, "Login Response: " + response.toString() );
            hideDialog();
            try {
                JSONObject jObj = new JSONObject( response );
                JSONObject jObjSuccess = jObj.getJSONObject( "success" );
                String token = jObjSuccess.getString( "token" );
                sp.setToken( token );
                // Create login session
                session.setLogin( true );
                checkUserDetails();
                checkLiked();

            } catch (JSONException e) {
                // JSON error
                e.printStackTrace();
            }
        }, error -> {
            Log.d( "error:", error.toString() );
            Log.e( TAG, "Login Error: " + error.getMessage() );
            Toast.makeText( getApplicationContext(), "email or password incorrect", Toast.LENGTH_LONG ).show();
            hideDialog();
        } ) {
            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put( "userNameOrEmail", email );
                params.put( "password", password );
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue( strReq1, tag_string_req1 );
    }

    // Show user details
    public void checkUserDetails() {
        // Tag used to cancel the request
        String tag_string_req2 = "show_detail";

        sp = new SharedPreferencesHandler( getApplicationContext() );

        StringRequest strReq2 = new StringRequest( Request.Method.GET,
                Config_URL.URL_DETAILS, response -> {
            Log.d( TAG, "detail Response: " + response.toString() );

            try {
                JSONObject jObj = new JSONObject( response );
                JSONObject jObjSucc = jObj.getJSONObject( "success" );
                String username = jObjSucc.getString( "username" );
                String email = jObjSucc.getString( "email" );
                sp.setName( username );
                sp.setEmail( email );
                Intent intent = new Intent( Activity_Login.this, Activity_User_details.class );
                startActivity( intent );
                finish();

            } catch (JSONException e) {
                // JSON error
                e.printStackTrace();
            }
        }, error -> {
            //*Log.d( "error:", error.toString() );
            Log.e( TAG, "Login Error: " + error.getMessage() );
            Toast.makeText( getApplicationContext(), "", Toast.LENGTH_LONG ).show();
        } ) {
            //This is for Headers If You Needed
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put( "Content-Type", "application/json" );
                headers.put( "Authorization", "Bearer " + sp.getToken( "token" ) );
                return headers;
            }
        };
        AppController.getInstance().addToRequestQueue( strReq2, tag_string_req2 );
    }

    public void checkLiked() {
        // Tag used to cancel the request
        String tag_string_req3 = "liked";

        sp = new SharedPreferencesHandler( getApplicationContext() );

        StringRequest strReq3 = new StringRequest( Request.Method.POST,
                Config_URL.URL_LIKE, (String response) -> {
            Log.d( TAG, "like Response: " + response.toString() );

            int mylike = 0;
            try {
                JSONObject jObj = new JSONObject( response );
                JSONObject jObjSucc = jObj.getJSONObject( "success" );
                String id = jObjSucc.getString( "id" );
                mylike = Integer.parseInt( id.toString() );
                sp.setId( id );
                Intent intent = new Intent( Activity_Login.this, Activity_User_details.class );
                startActivity( intent );
                finish();

            } catch (JSONException e) {
                // JSON error
                System.out.println( "Could not parse " + e );
                e.printStackTrace();
            }
        }, error -> {
            //*Log.d( "error:", error.toString() );
            Log.e( TAG, "Login Error: " + error.getMessage() );
            Toast.makeText( getApplicationContext(), "", Toast.LENGTH_LONG ).show();
        } ) {
            //This is for Headers If You Needed
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put( "id", "id " );
                return headers;
            }
        };
        AppController.getInstance().addToRequestQueue( strReq3, tag_string_req3 );
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}