package login.rbny.com.radio_bangla_ny.livehost;

import android.content.Context;
import android.os.Handler;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;

import java.util.List;

import login.rbny.com.radio_bangla_ny.R;

public class LiveHostsAdapter extends RecyclerView.Adapter<LiveHostsAdapter.MyViewHolder> {

    private List<LiveHostsData> liveHostsList;
    //private List<HostList> hostLists;
    private Context context;
    private LayoutInflater inflater;


    public LiveHostsAdapter(Context context, List<LiveHostsData> liveHostsList) {

        this.context = context;
        this.liveHostsList = liveHostsList;
        inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
    }

    public LiveHostsAdapter(List<LiveHostsData> horizontalList) {
        this.liveHostsList = horizontalList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = inflater.inflate( R.layout.liveprogramhosts, parent, false );
        return new MyViewHolder( itemView );
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        LiveHostsData liveHostIs = liveHostsList.get( position );
        //Pass the values of upcoming object to Views
        holder.display_name.setText( liveHostIs.getDisplay_name() );
        holder.imageview.setImageUrl( liveHostIs.getImageURL(), NetworkController_live.getInstance( context ).getImageLoader() );
    }

    @Override
    public int getItemCount() {
        return liveHostsList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView display_name;
        private NetworkImageView imageview;

        public MyViewHolder(View itemView) {
            super( itemView );
            display_name = (TextView) itemView.findViewById( R.id.display_name );
            // Volley's NetworkImageView which will load Image from URL
            imageview = (NetworkImageView) itemView.findViewById( R.id.thumbnail );
        }
    }
}