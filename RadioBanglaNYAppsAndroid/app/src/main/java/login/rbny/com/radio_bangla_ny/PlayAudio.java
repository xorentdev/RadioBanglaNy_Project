package login.rbny.com.radio_bangla_ny;

import android.app.ProgressDialog;
import android.content.Context;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import java.io.IOException;

/**
 * Created by yksprasad on 14/12/17.
 */

public class PlayAudio extends ProgressDialog {
    Context mcontext;
    public static MediaPlayer mediaPlayer;
    public static boolean isplayingAudio = false;
    private static String STREAM_URL = "https://stream.radio.co/s57864048c/listen";
    public static boolean isdialog=false;

    public PlayAudio(@NonNull Context context) {
        super(context);
        mcontext = context;
    }

    public void audiopreapare() {
        mediaPlayer = new MediaPlayer();
        new PlayerTask().execute(STREAM_URL);
    }

    private class PlayerTask extends AsyncTask<String, Void, Boolean> {
        @Override
        protected void onPreExecute() {

        }
        @Override
        protected Boolean doInBackground(String... strings) {
            Boolean status=false;
            try {
                // Preparing mediaplayer
                mediaPlayer.setDataSource(STREAM_URL);
                mediaPlayer.prepareAsync();
                status=true;
            } catch (IOException e) {
                mediaPlayer.reset();
                e.printStackTrace();
            }
            return status;
        }
        @Override
        protected void onPostExecute(Boolean aBoolean) {

            super.onPostExecute(aBoolean);


            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mediaPlayer) {

                    isdialog=true;
                    isplayingAudio = true;
                    mediaPlayer.start();
                }
            });
        }
    }

    public static void stopAudio() {
        isplayingAudio = false;
        mediaPlayer.stop();
    }

    public static void pause() {
        isplayingAudio = false;
        mediaPlayer.pause();
    }

    public static void start() {
        isplayingAudio = true;
        mediaPlayer.start();
    }


    public static void stop() {
        isplayingAudio = true;
        mediaPlayer.stop();
    }

    public static void release() {
        isplayingAudio = true;
        mediaPlayer.release();
    }
}
