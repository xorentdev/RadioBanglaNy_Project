package login.rbny.com.radio_bangla_ny.livehost;


public class LiveHostsData {

    private static String API_BASE_URL = "https://radiobangla.us/";
    private String display_name;
    private String imageURL;

    public LiveHostsData(String display_name, String image) {

        this.imageURL = image;
        this.display_name = display_name;
    }

    public String getDisplay_name() {
        return display_name;
    }
    public String getImageURL() {
        if (imageURL != null) {
            return API_BASE_URL+"uploads/users/original/" + imageURL;
        } else {
            return API_BASE_URL+"uploads/images/default/rj_profile.jpg";
        }
    }

}




