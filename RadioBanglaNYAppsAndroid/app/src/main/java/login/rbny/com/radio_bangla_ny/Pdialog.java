package login.rbny.com.radio_bangla_ny;

import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by yksprasad on 14/12/17.
 */

public class Pdialog {
    // Progress dialogue
    static ProgressDialog m_Dialog;
    Long time;
    public static ProgressDialog showProgressDialog(Context context, String message){
         m_Dialog = new ProgressDialog(context);
        m_Dialog.setMessage(message);
        m_Dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        m_Dialog.setCancelable(false);
        m_Dialog.show();


        return m_Dialog;
    }

    public static ProgressDialog dismissprogress(Context context){

        m_Dialog.dismiss();
        return m_Dialog;
    }
}
